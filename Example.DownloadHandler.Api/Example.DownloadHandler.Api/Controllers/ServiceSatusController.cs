﻿using System.Web.Http;
using Example.DownloadHandler.Api.Contracts;
using Example.DownloadHandler.Api.Models;
using log4net;

namespace Example.DownloadHandler.Api.Controllers
{
    public class ServiceSatusController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(ServiceSatusController));


        [RoutePrefix("downloadhandler")]
        public class ServiceStatusController : ApiController
        {
          
            private IServiceStatus ServiceStatusData { get; }

            public ServiceStatusController()
            {
                ServiceStatusData = new Example.DownloadHandler.Api.Service.ServiceStatus();

            }

            [Route("servicestatus")]
            public ServiceStatus Get()
            {
                var svcStatus = ServiceStatusData.GetServiceStatus();
                Log.InfoFormat($"Get ServiceStatus:{svcStatus.IsUp}, ServerTime:{svcStatus.ServerTime} ");
                return svcStatus;
            }
        }
    }
}