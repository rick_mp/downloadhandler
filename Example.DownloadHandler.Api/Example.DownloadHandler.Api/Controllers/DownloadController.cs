﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Example.DownloadHandler.Api.Service;
using log4net;

namespace Example.DownloadHandler.Api.Controllers
{



    [RoutePrefix("downloadhandler")]
    public class DownloadController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(DownloadController));

        [HttpGet]
        [HttpHead]
        [Route("getinfofile/{*id}")]
        public async Task<HttpResponseMessage> GetInfoFile(string id)
        {

            var file = await DownloadService.GetDownloadParametersAsync(id);
            Log.DebugFormat($"Get GetInfoFile:{id}");
            var response = Request.CreateResponse(HttpStatusCode.OK);
            await Task.Run(() =>
            {
                var fileName = Path.GetFileName(file);
                Log.DebugFormat($"fileName:{fileName}");
                if (string.IsNullOrEmpty(fileName))
                    throw new Exception("File name '" + fileName + "' is not valid.");

                var fi = new FileInfo(file);
                var fileLength = fi.Length;
                Log.DebugFormat($"fileLength:{fileLength}");

                response.Headers.Add("X-Content-Length", fileLength.ToString());
                response.Headers.Add("X-Accept-Ranges", "bytes");


                return response;

            });

            return response;
        }


        [HttpGet]
        [Route("downloadfile/{*id}")]
        public async Task<HttpResponseMessage> DownloadFile(string id)
        {

            var file = await DownloadService.GetDownloadParametersAsync(id);
            Log.DebugFormat($"DownloadFile:{id}");
            var response = Request.CreateResponse(HttpStatusCode.OK);

           var s= await Task.Run(() =>
            {
                var fileName = Path.GetFileName(file);
                Log.DebugFormat($"fileName:{fileName}");
                if (string.IsNullOrEmpty(fileName))
                    throw new Exception("File name '" + fileName + "' is not valid.");

                var fi = new FileInfo(file);
                var fileLength = fi.Length;
                Log.DebugFormat($"fileLength:{fileLength}");


                
                response.Content = new StreamContent(new FileStream(file, FileMode.Open));
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileName

                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response.Content.Headers.Expires = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0));

                //file save

                response.Headers.Add("X-Content-Encoding", Encoding.UTF8.ToString());


                // response.Headers.Add("X-Content-Disposition", "attachment;filename=\"" + fileName + "\"");
                // response.Headers.Add("X-Content-Type", "application/octet-stream");
                response.Headers.Add("X-Cache-Control", "no-store, no-cache");

                //  response.ExpiresAbsolute = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0));
                //response.Expires = -1;

                



                return response;

            });



            return response;
        }


        public async void WriteToStream(Stream outputStream, HttpContent content, TransportContext context)
        {
            try
            {
                var _filename = "";
                var buffer = new byte[65536];

                using (var video = File.Open(_filename, FileMode.Open, FileAccess.Read))
                {
                    var length = (int)video.Length;
                    var bytesRead = 1;

                    while (length > 0 && bytesRead > 0)
                    {
                        bytesRead = video.Read(buffer, 0, Math.Min(length, buffer.Length));
                        await outputStream.WriteAsync(buffer, 0, bytesRead);
                        length -= bytesRead;
                    }
                }
            }
            catch (HttpException ex)
            {
                return;
            }
            finally
            {
                outputStream.Close();
            }
        }
    }



}
