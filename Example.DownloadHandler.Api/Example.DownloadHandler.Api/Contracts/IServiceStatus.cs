﻿using Example.DownloadHandler.Api.Models;

namespace Example.DownloadHandler.Api.Contracts
{
   public interface IServiceStatus
    {
        ServiceStatus GetServiceStatus();
    }
}
