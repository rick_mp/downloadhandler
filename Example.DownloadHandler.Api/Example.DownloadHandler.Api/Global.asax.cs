﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Example.DownloadHandler.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {


            AreaRegistration.RegisterAllAreas();
            HttpConfiguration config = GlobalConfiguration.Configuration;
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            config.EnsureInitialized();
            //HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);

        }

       
    }
}
