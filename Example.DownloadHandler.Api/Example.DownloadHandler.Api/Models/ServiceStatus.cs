﻿using System;

namespace Example.DownloadHandler.Api.Models
{
    public class ServiceStatus
    {
        public bool IsUp { get; set; }
        public DateTime ServerTime { get; set; }
    }
}