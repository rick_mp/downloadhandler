﻿using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;

namespace Example.DownloadHandler.Api.Service
{
    public static class DownloadService
    {

        public static async Task<string> GetDownloadParametersAsync(string fileName)
        {
            var exportFilePath= await Task.Run(() =>
            {
                if (string.IsNullOrEmpty(fileName))
                    throw new Exception("Must specify file in query string.  (Example: Download/yourfile)");

              
                var file= ConfigurationManager.AppSettings["MasterExportDirectory"] + "\\" + fileName;


                //var fileName = Path.GetFileName(file);
                //if (string.IsNullOrEmpty(fileName))
                //    throw new Exception("File name '" + decryptedFileName + "' is not valid.");



                //long chunkSize = 0;
                //if (!string.IsNullOrEmpty(context.Request.QueryString["ChunkSize"]))
                //    if (context.Request.QueryString["ChunkSize"].Trim().Length > 0)
                //        chunkSize = long.Parse(context.Request.QueryString["ChunkSize"]);

                FileInfo fi = new FileInfo(file);
                long fileLength = fi.Length;
                //if (chunkSize > 0 && chunkSize > fileLength)
                //    throw new Exception("ChunkSize is greater than file length.");



                return file;

            });
            return exportFilePath;
        }
    }
}
