﻿using System;
using Example.DownloadHandler.Api.Contracts;

namespace Example.DownloadHandler.Api.Service
{
    public class ServiceStatus:IServiceStatus
    {
        public Models.ServiceStatus GetServiceStatus()
        {

            return new Models.ServiceStatus { ServerTime = DateTime.UtcNow, IsUp = true };
        }
    }
}