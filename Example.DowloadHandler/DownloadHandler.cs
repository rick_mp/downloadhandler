﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using IOThreads;

namespace DownloadHandlers
{
    ////////////////////////////////////////////////////
    // Example URLs to call download handler and download a file:
    //
    //    https://localhost/DownloadPortal/Download?file=file.txt
    //    https://localhost/DownloadPortal/Download?file=file.txt&chunksize=1000000
    //    https://localhost/DownloadPortal/Download?file=customer1/file.txt
    //    https://localhost/DownloadPortal/Download?file=customer1/file.txt&chunksize=1000000
    //
    //    Important!!!  Must replace 'localhost' in URL with actual machine name or SSL certificate will fail.
    //
    //    Can either include the 'Range' HTTP request header in the HTTP web request OR
    //    supply the 'chunksize' parameter in the URL. If include the 'Range' HTTP request header,
    //    it will transmit back that portion of the file identified by the 'Range'. If supply the
    //    'chunksize' parameter, it will transmit back the entire file in separate pieces the size of
    //    'chunksize'. If supply both, 'Range' will be used. If supply neither, it will transmit back
    //    the entire file in one piece.
    //
    // Web.config on IIS 7.0:
    //    <system.webServer>
    //      <handlers>
    //        <add name="Download" verb="*" path="Download" type="DownloadHandlers.DownloadHandler" />
    //      </handlers>
    //    </system.webServer>
    //
    // Put DownloadHandler.dll and IOThreads.dll in the /bin directory of the virtual directory on IIS.
    //
    // Put files to be downloaded into virtual directory or sub directory under virtual directory.
    //
    // To debug:
    //    Debug \ Attach to Process \ w3wp.exe
    //
    // Note: Make sure the IIS virtual directory is using an AppPool that is using .NET Framework 4.0.
    //       Define a new AppPool using .NET Framework 4.0, if no other AppPool exists that is using .NET Framework 4.0.
    ////////////////////////////////////////////////////

    public class DownloadHandler : IHttpAsyncHandler
    {
        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            //Offloads to "completion port threads" inside CLR ThreadPool
            //instead of "worker threads" inside CLR ThreadPool, so that our
            //work doesn't use up the "worker threads" that are needed for the
            //IIS server's request processing. (see comment section at top of
            //IOThread class for more details)
            IoThread.ProcessRequestDelegate d = ProcessRequest;
            return d.IoBeginInvoke(context, cb, extraData);
        }

        public void EndProcessRequest(IAsyncResult result)
        {
            IoThread.IoAsyncResult ar = (IoThread.IoAsyncResult)result;
            IoThread.ProcessRequestDelegate d = (IoThread.ProcessRequestDelegate)ar.AsyncDelegate;
            d.IoEndInvoke(result);
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                
                
                string file = context.Request.QueryString["File"];

                string fileName = Path.GetFileName(file);
                if (string.IsNullOrEmpty(fileName))
                    throw new Exception("File name '" + fileName + "' is not valid.");

                long chunkSize = 0;
                if (!string.IsNullOrEmpty(context.Request.QueryString["ChunkSize"]))
                    if (context.Request.QueryString["ChunkSize"].Trim().Length > 0)
                        chunkSize = long.Parse(context.Request.QueryString["ChunkSize"]);

                FileInfo fi = new FileInfo(file);
                long fileLength = fi.Length;
                if (chunkSize > 0 && chunkSize > fileLength)
                    throw new Exception("ChunkSize is greater than file length.");


                context.Response.ClearHeaders();
                context.Response.ClearContent();
                context.Response.Clear();

                //request is just checking file length
                if (context.Request.HttpMethod == "HEAD")
                {
                    context.Response.AddHeader("content-length", fileLength.ToString());
                    context.Response.AddHeader("accept-ranges", "bytes");
                    context.Response.Flush();
                    return;
                }
                
                //file save
                context.Response.ContentEncoding = Encoding.UTF8;
                context.Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
                //context.Response.AddHeader("content-disposition", "attachment;filename=\"" + HttpUtility.UrlEncode(fileName) + "\"");
                context.Response.ContentType = "application/octet-stream";
                context.Response.AddHeader("cache-control", "no-store, no-cache");
                context.Response.ExpiresAbsolute = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0));
                context.Response.Expires = -1;
                context.Response.AddHeader("Connection", "Keep-Alive");
                context.Response.AddHeader("accept-ranges", "bytes");

                //request is for byte range
                if (!string.IsNullOrEmpty(context.Request.Headers["Range"]) && context.Request.Headers["Range"].Trim().Length > 0)
                {
                    string range = context.Request.Headers["Range"];
                    range = range.Replace(" ", "");
                    string[] parts = range.Split(new char[] { '=', '-' });
                    long contentLength = long.Parse(parts[2]) - long.Parse(parts[1]);
                    context.Response.AddHeader("content-length", contentLength.ToString());
                    string contentRange = $"bytes {parts[1]}-{parts[2]}/{fileLength.ToString()}";
                    context.Response.AddHeader("content-range", contentRange);
                    context.Response.AddHeader("last-modified", DateTime.Now.ToString("ddd, dd MMM yyyy hh:mm:ss") + " GMT");
                    byte[] bytes = Encoding.ASCII.GetBytes($"{fi.Name} : {fi.LastAccessTimeUtc}");
                    string eTag = Convert.ToBase64String(MD5CryptoServiceProvider.Create().ComputeHash(bytes));
                    context.Response.AddHeader("ETag", $"\"{eTag}\"");
                    context.Response.StatusCode = 206;  //partial content

                    context.Response.TransmitFile(file, long.Parse(parts[1]), contentLength);
                    context.Response.Flush();
                }
                else  //request is not for byte range
                {
                    context.Response.AddHeader("content-length", fileLength.ToString());

                    if (chunkSize <= 0)
                    {
                        context.Response.TransmitFile(file);
                        context.Response.Flush();
                    }
                    else
                    {
                        long index = 0;
                        while (true)
                        {
                            if (index >= fileLength)
                                break;
                            if (index < fileLength && index + chunkSize > fileLength)
                            {
                                context.Response.TransmitFile(file, index, fileLength - index);
                                context.Response.Flush();
                                break;
                            }

                            context.Response.TransmitFile(file, index, chunkSize);
                            context.Response.Flush();
                            index += chunkSize;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                context.Response.ClearHeaders();
                context.Response.ClearContent();
                context.Response.Clear();

                context.Response.StatusCode = 500;
                context.Response.Write("Download Error:  " + ex.GetBaseException().Message);
                context.Response.Flush();
                //throw ex;
            }
            finally
            {
                context.Response.End();
                //context.ApplicationInstance.CompleteRequest();
            }
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }

    
  


}
