﻿using System;
using System.Threading;
using System.Web;

namespace IOThreads
{
    ////////////////////////////////////////////////////
    // A delegate's BeginInvoke runs on a "worker thread" inside the CLR ThreadPool.
    // This allows you to run a delegate on a "completion port thread" inside the CLR ThreadPool
    // instead of a "worker thread" by calling IOBeginInvoke instead of BeginInvoke.
    // In a nut shell, this mechanism basically skips the actual system I/O and simply posts a
    // completion delegate to the completion port queue so the completion delegate will be 
    // executed by a "completion port thread" that is working the completion port queue.
    //
    // Example:
    // delegate.BeginInvoke => executes delegate on "worker thread".
    // delegate.IOBeginInvoke => executes delegate on "completion port thread".
    // 
    //
    // Extremely simplified explanation:
    //
    // CLR ThreadPool is made up of two pools of threads: "worker threads" and "completion port threads".
    //
    // Basically you can either queue a user work item which runs the delegate on a "worker thread",
    // or queue a native overlapped item which runs the completion delegate on a "completion port thread".
    // ThreadPool.QueueUserWorkItem (and delegate.BeginInvoke) => executes delegate on "worker thread".
    // ThreadPool.UnsafeQueueNativeOverlapped => executes completion delegate on "completion port thread".
    //
    //             (CLR ThreadPool)
    //               /        \
    // [worker threads]      [completion port threads]
    //
    //  o o  oo  o  oo                    _____________post to queue using ThreadPool.UnsafeQueueNativeOverlapped
    //    o o oo o  o                    |             (i.e. PostQueuedCompletionStatus)
    //   o  o o  o o                     v
    //  oo o  oo  o o                  |  |
    //    o  oo oo o                   |  | <----------completion port queue (one per process)
    //                                 |__|
    //       ^                         oo o
    //       |                        o o oo <---------completion port threads (work the completion port queue)
    //       |                 (cpu)(cpu)(cpu)(cpu)                            (i.e. GetQueuedCompletionStatus in loop)
    //       |                               
    //       |                               ^
    //       |                               |
    //       |                               |
    //       |                               |
    //       |                               |
    //       |    Each individual completion delegate is given to the completion port queue to execute,
    //       |    and the "completion port threads" working the completion port queue execute the delegate.
    //       |    (This has much less risk of thread explosion because each delegate just gets posted to the
    //       |     completion port queue, instead of being given to its own individual thread. Basically
    //       |     the queue grows, not the threads.)
    //       |
    //       |    The completion delegate is supplied in the overlapped structure that is posted to the
    //       |    completion port queue.
    //       |
    //       |    Posting to queue (PostQueuedCompletionStatus) => done by ThreadPool.UnsafeQueueNativeOverlapped.
    //       |    Working queue (GetQueuedCompletionStatus in loop) => done by "completion port thread" working queue.
    //       |
    //       |
    // Each individual delegate is given to its own individual "worker thread" to execute,
    // and the OS schedules the "worker thread" to run on a cpu.
    // (This has the risk of thread explosion because each new delegate executes on its own
    //  individual "worker thread". If the number of threads grows to large the entire OS can
    //  grind to a hault, with all the memory used and the cpu's spending all their time
    //  just trying to schedule the threads to run but not actually doing any work.)
    //
    ////////////////////////////////////////////////////
    public static class IoThread
    {
        public delegate void ProcessRequestDelegate(HttpContext context);

        public class IoAsyncResult : IAsyncResult
        {
            public object AsyncState { get; set; }
            public WaitHandle AsyncWaitHandle { get; set; }
            public Delegate AsyncDelegate { get; set; }
            public bool CompletedSynchronously { get; set; }
            public bool IsCompleted { get; set; }
            public Exception Exception { get; set; }
        }

        public static unsafe IAsyncResult IoBeginInvoke(this ProcessRequestDelegate value, HttpContext context, AsyncCallback callback, object data)
        {
            ManualResetEvent evt = new ManualResetEvent(false);

            IoAsyncResult ar = new IoAsyncResult();
            ar.AsyncState = new[] { context, callback, data };
            ar.AsyncDelegate = value;
            ar.AsyncWaitHandle = evt;

            Overlapped o = new Overlapped(0, 0, IntPtr.Zero, ar);
            NativeOverlapped* no = o.Pack(ProcessRequestCompletionCallback, data);
            ThreadPool.UnsafeQueueNativeOverlapped(no);

            return ar;
        }

        private static unsafe void ProcessRequestCompletionCallback(uint errorCode, uint numBytes, NativeOverlapped* no)
        {
            try
            {
                Overlapped o = Overlapped.Unpack(no);

                ProcessRequestDelegate d = (ProcessRequestDelegate)((IoAsyncResult)o.AsyncResult).AsyncDelegate;

                object[] state = (object[])o.AsyncResult.AsyncState;
                HttpContext context = (HttpContext)state[0];
                AsyncCallback callback = (AsyncCallback)state[1];
                object data = state[2];

                try
                {
                    d(context);
                }
                catch(Exception ex)
                {
                    ((IoAsyncResult)o.AsyncResult).Exception = ex;
                }

                ((IoAsyncResult)o.AsyncResult).IsCompleted = true;
                ((ManualResetEvent)o.AsyncResult.AsyncWaitHandle).Set();

                if (callback != null)
                    callback(o.AsyncResult);
            }
            finally
            {
                Overlapped.Free(no);
            }
        }

        public static void IoEndInvoke(this ProcessRequestDelegate value, IAsyncResult result)
        {
            IoAsyncResult ar = (IoAsyncResult)result;
            ar.AsyncWaitHandle.WaitOne(Timeout.Infinite);
            if (ar.Exception != null)
                throw ar.Exception;
        }
    }
}
