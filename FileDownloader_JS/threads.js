﻿/**
* threads.js
* Author: eherrera
* Date: 09/28/2010
*
*/

function thrLoop(config) {
    var delay = .005;
    var self = this;

    config = config || {};

    var collection = [];
    if (config.collection) {
        collection = config.collection;
    }

    var handler_finish = function () { };
    if (config.finish) {
        handler_finish = config.finish;
    }

    var MessageException = 'There is no action in which iterate';
    var handler_action = function () { if ($Debug){if (console.error) { console.error(MessageException); } else { throw MessageException; }} };
    if (config.action) {
        handler_action = config.action;
    }

    var interval = 1;
    if (config.interval) {
        interval = config.interval;
    }

    var total = collection.length;

    var thread = {
        work: null,
        wait: null,
        index: 0,
        total: total,
        finished: false
    };

    self.collection = collection;
    self.finish = handler_finish;
    self.action = handler_action;
    self.interval = interval;
    var chunk = parseInt(total * delay);
    self.chunk = (chunk == NaN || chunk == 0) ? total : chunk;

    thread.clear = function () {
        window.clearInterval(thread.work);
        window.clearTimeout(thread.wait);
        thread.work = null;
        thread.wait = null;
    };

    thread.end = function () {
        if (thread.finished) { return; }
        self.finish();
        thread.finished = true;
    };

    thread.process = function () {
        if (thread.index >= thread.total) { return false; }

        if (thread.work) {
            var part = Math.min((thread.index + self.chunk), thread.total);
            while (thread.index < part) {
                if (self.collection[thread.index]) {
                    self.action(self.collection[thread.index], thread.index, thread.total);
                }
                thread.index++;
            }
        } else {
            while (thread.index < thread.total) {
                if (self.collection[thread.index]) {
                    self.action(self.collection[thread.index], thread.index, thread.total);
                }
                thread.index++;
            }
        }

        if (thread.index >= thread.total) {
            thread.clear();
            thread.end();
        }

        return true;
    };

    self._handle_start = function () {
        thread.finished = false;
        thread.index = 0;
        thread.work = window.setInterval(thread.process, self.interval);
    };

    self._handle_wait = function (timeout) {

        var complete = function () {
            thread.clear();
            thread.process();
            thread.end();
        };

        if (!timeout) {
            complete();
        } else {
            thread.wait = window.setTimeout(complete, timeout);
        }
    };

    return {

        start: self._handle_start,

        wait: self._handle_wait

    };
}